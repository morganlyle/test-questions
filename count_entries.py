# This function count_entries will take a string that is composed of values that are separated by some "delimiter" character, like a comma. It then returns the number of values in that string that are separated by the provided delimiter.

def count_entries(line, separator):
    # your code here

    result = len(line.split(separator))

    return result
