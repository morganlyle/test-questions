# In credit cards, network messages, and other kinds of data, there is usually an extra piece of information called a checksum. The checksum allows you to quickly check to see if the data is valid.

# Implement the checksum method using the following algorithm.

# Start with a sum of zero
# For each letter in the message
# Convert the letter to a number using the ord function
# Add the number to the sum
# Use modulo division % to find the remainder when the sum is divided by 26
# Add 97 to the remainder
# Convert the sum from the previous step to a letter using the chr function
# Return that letter
# Here are some examples of inputs and outputs.

def checksum(message):
    # your code here

    sum = 0

    for letter in message:

        new_num = ord(letter)

        sum = new_num + sum

    new_message = chr((sum % 26) + 97)

    return new_message
