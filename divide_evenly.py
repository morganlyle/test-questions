# You can test if a number is an integer by using the int function.

# if int(x) == x:    # This is true if x is an integer
#     print("I am integer!")
# This function takes two values: numerator and denominator. If the quotient (the numerator divided by the denominator) is an integer, then this function returns True. Otherwise, it returns False.


def divides_evenly(numerator, denominator):
    # your code here

    if int(numerator) % 2 == 0:
        return True

    elif int(denominator) % 2 == 0:
        return False


numerator = int(6)
denominator = int(2)

divides_evenly(numerator, denominator)

print(divides_evenly(numerator, denominator))
