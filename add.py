# This function, add, will calculate the sum of all of the items in the input list, items.

# If an item is a number it can be added to the sum normally.
# If it is a string, it should be converted to a number using the float(...) function, if possible, and then added to the sum.
# If the string cannot be converted to a number, you should just ignore the error and not add it to the sum.
# The function below works when items only has numbers in it, but fails if there are any strings in it. items will only contain numbers and strings.

# Fix the code below to handle non-string items according to the rules above.


def add(items):
    sum = 0
    for item in items:
        try:
            sum += float(item)
        except:
            ValueError
    return sum
