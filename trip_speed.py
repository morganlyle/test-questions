# The following function calculates the average speed for a long, multi-segment road-trip. It takes a list of trip segments where each segment has the number of miles traveled and the number of hours it took to travel that distance, like this:

trip = [
    {"miles": 300, "hours": 5},
    {"miles": 120, "hours": 1.5},
]
# The code below works!, sometimes...

# If an incalculable situation arises, the function should return this value: float("nan")

# Please update the function below so that it passes all of the unit tests.


def average_trip_speed(trip):
    total_miles = 0
    total_hours = 0

    for leg in trip:
        total_miles += leg["miles"]
        total_hours += leg["hours"]
    if total_hours == 0:
        return float("nan")

    return total_miles / total_hours


average_trip_speed(trip)

print(average_trip_speed(trip))
